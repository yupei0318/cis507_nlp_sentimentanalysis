I have changed my project from designing a specified search engine over Wikipedia to conducting sentiment analysis using python, nltk and data streams from twitter.

This is a individual project and the milestones are listed as follows:

Milestone1: Install nltk, a widely used open source project for natural language processing. Steaming tweets data from Twitter API and conduct sentimental analysis for certain words.

Milestone2: Visualize the results of the sentimental analysis above. Conduct real-time twitter sentiment analysis and experiment with different algorithms to see which one has the best performance in terms of accuracy.